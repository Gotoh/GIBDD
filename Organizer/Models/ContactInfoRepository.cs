﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Organizer.Models
{
    public class ContactInfoRepository : IRepository<ContactInfo>
    {
        private OrganizerContext db;

        public ContactInfoRepository(OrganizerContext context)
        {
            db = context;
        }

        public IEnumerable<ContactInfo> Get(Expression<Func<ContactInfo, bool>> filter = null, Func<IQueryable<ContactInfo>, IOrderedQueryable<ContactInfo>> orderBy = null)
        {
            IQueryable<ContactInfo> query = db.ContactInfo;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public void Update(ContactInfo contact)
        {
            db.Entry(contact).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Contacts contact = db.Contacts.Find(id);
            if (contact != null)
                db.Contacts.Remove(contact);
        }

        public void Add(ContactInfo contact)
        {
            db.ContactInfo.Add(contact);
        }
    }
}