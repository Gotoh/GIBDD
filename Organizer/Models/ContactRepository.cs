﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Organizer.Models
{
    public class ContactRepository : IRepository<Contacts>
    {
        private OrganizerContext db;

        public ContactRepository(OrganizerContext context)
        {
            db = context;
        }

        public IEnumerable<Contacts> Get(Expression<Func<Contacts, bool>> filter = null,
            Func<IQueryable<Contacts>, IOrderedQueryable<Contacts>> orderBy = null)
        {
            IQueryable<Contacts> query = db.Contacts;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public void Update(Contacts contact)
        {
            db.Entry(contact).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Contacts contact = db.Contacts.Find(id);
            if (contact != null)
                db.Contacts.Remove(contact);
        }

        public void Add(Contacts contact)
        {
            db.Contacts.Add(contact);
        }

        public IEnumerable<ContactView> Search(string search)
        {
            try
            {
                var contacts = db.Contacts.Where(x => x.Name.IndexOf(search) >=0 || x.Surname.IndexOf(search) >= 0 ||
                    x.Patronimyc.IndexOf(search) >= 0 || x.Organization.IndexOf(search) >= 0 || x.Post.IndexOf(search) >= 0);



                var contactInfo =
                    db.ContactInfo.Where(
                        x => x.Value.IndexOf(search) != -1 && !contacts.Any(k => k.Id == x.ContactId));


                var contactsview = contacts.Select(z => new ContactView()
                {
                    Contact = z,
                    ContactInfo = db.ContactInfo.Where(x => x.ContactId == z.Id).ToList()
                }).ToList();

                foreach (var b in contactInfo)
                {
                    if (!contactsview.Any(x => x.Contact.Id == b.ContactId))
                    {
                        contactsview.Add(new ContactView()
                        {
                            Contact = db.Contacts.FirstOrDefault(x => x.Id == b.ContactId),
                            ContactInfo = db.ContactInfo.Where(x => x.ContactId == b.ContactId).ToList()
                        });

                    }

                }
                return contactsview;
            }
            catch (Exception er)
            {
                
                throw;
            }
         


        }
    }
}