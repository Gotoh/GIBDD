﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Organizer.Models
{
    public class RecordRepository : IRepository<Records>
    {
        private OrganizerContext db;

        public RecordRepository(OrganizerContext context)
        {
            db = context;
        }

        public IEnumerable<Records> Get(Expression<Func<Records, bool>> filter = null, Func<IQueryable<Records>, IOrderedQueryable<Records>> orderBy = null)
        {
            IQueryable<Records> query = db.Records;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public void Update(Records record)
        {
            db.Entry(record).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Records record = db.Records.Find(id);
            if (record != null)
                db.Records.Remove(record);
        }

        public void Add(Records record)
        {
            db.Records.Add(record);
        }
    }
}