namespace Organizer.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
 

    public class Records
    {
        public int Id { get; set; }

        public int Type { get; set; }

        [Required]
        [StringLength(100)]
        public string Theme { get; set; }

        public DateTime DateTimeStart { get; set; }

        public DateTime? DateTimeEnd { get; set; }

        [StringLength(250)]
        public string Place { get; set; }

        public bool Done { get; set; }
    }
}
