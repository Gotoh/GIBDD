﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Organizer.Models
{
    public class UnitOfWork : IDisposable
    {
        private OrganizerContext db = new OrganizerContext();
        private RecordRepository recordRepository;
        private ContactRepository contactRepository;
        private ContactInfoRepository contactInfoRepository;

        public RecordRepository Records
        {
            get
            {
                if (recordRepository == null)
                    recordRepository = new RecordRepository(db);
                return recordRepository;
            }
        }

        public ContactRepository Contacts
        {
            get
            {
                if (contactRepository == null)
                    contactRepository = new ContactRepository(db);
                return contactRepository;
            }
        }

        public ContactInfoRepository ContactInfo
        {
            get
            {
                if (contactInfoRepository == null)
                    contactInfoRepository = new ContactInfoRepository(db);
                return contactInfoRepository;
            }
        }
        public void Save()
        {
            db.SaveChanges();
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}