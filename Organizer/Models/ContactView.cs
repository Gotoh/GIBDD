﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Organizer.Models
{
    public class ContactView
    {
        public Contacts Contact { get; set; }
        public List<ContactInfo> ContactInfo { get; set; }
    }
}