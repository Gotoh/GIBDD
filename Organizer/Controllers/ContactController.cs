﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Organizer.Models;

namespace Organizer.Controllers
{
    public class ContactController : Controller
    {
        UnitOfWork unitOfWork;
        public ContactController()
        {
            unitOfWork = new UnitOfWork();
        }
        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult SearchContact(string search)
        {
            if (search == "")
            {
                ViewBag.Contacts = unitOfWork.Contacts.Get(null, ord => ord.OrderBy((x => x.Surname))).Select(x => new ContactView() { Contact = x, ContactInfo = unitOfWork.ContactInfo.Get(inf => inf.ContactId == x.Id).ToList() });
            }
            else
            {
                ViewBag.Contacts = unitOfWork.Contacts.Search(search);
            }
           
            return PartialView("_Contacts");
        }

        public ActionResult AddContact()
        {
            ViewBag.Record = null;
            return PartialView("_Contact");

        }

        [HttpPost]
        public ActionResult AddContact(Contacts contact)
        {
            try
            {
                unitOfWork.Contacts.Add(contact);
                unitOfWork.Save();
                ViewBag.Contacts = unitOfWork.Contacts.Get(null, ord => ord.OrderBy((x => x.Surname))).Select(x => new ContactView() { Contact = x, ContactInfo = unitOfWork.ContactInfo.Get(inf => inf.ContactId == x.Id).ToList() });

                return PartialView("_Contacts");
            }
            catch (Exception)
            {

                return PartialView("_Contacts");
            }

        }

        public ActionResult UpdateContact(int contactId)
        {
            ViewBag.Contact = unitOfWork.Contacts.Get(x => x.Id == contactId).FirstOrDefault();
            return PartialView("_Contact");

        }

        [HttpPost]
        public ActionResult UpdateContact(Contacts contact)
        {
            try
            {
                var con= unitOfWork.Contacts.Get(x => x.Id == contact.Id).FirstOrDefault();
                if (con != null)
                {
                    con.Birthday = contact.Birthday;
                    con.Name = contact.Name;
                    con.Organization = contact.Organization;
                    con.Patronimyc = contact.Patronimyc;
                    con.Post = contact.Post;
                    con.Surname = contact.Surname;
                }
                unitOfWork.Contacts.Update(con);
                unitOfWork.Save();
                ViewBag.Contacts = unitOfWork.Contacts.Get(null, ord => ord.OrderBy((x => x.Surname))).Select(x => new ContactView() { Contact = x, ContactInfo = unitOfWork.ContactInfo.Get(inf => inf.ContactId == x.Id).ToList() });

                return PartialView("_Contacts");
            }
            catch (Exception)
            {

                return PartialView("_Contacts");
            }

        }

        [HttpPost]
        public bool DeleteCOntact(int contactId)
        {
            unitOfWork.Contacts.Delete(contactId);
            unitOfWork.Save();
            return true;

        }
    }
}