﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Organizer.Models;

namespace Organizer.Controllers
{
    public class DiaryController : Controller
    {
        UnitOfWork unitOfWork;
        public DiaryController()
        {
            unitOfWork = new UnitOfWork();
        }
        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            ViewBag.Records = unitOfWork.Records.Get(record => record.DateTimeStart == DateTime.Today,ord => ord.OrderBy((x => x.DateTimeStart)));
           ViewBag.Contacts= unitOfWork.Contacts.Get(null, ord => ord.OrderBy((x => x.Surname))).Select(x=> new ContactView() {Contact = x, ContactInfo = unitOfWork.ContactInfo.Get(inf=>inf.ContactId==x.Id).ToList()});

            return View();
        }


        public ActionResult GetRecords(string viewParams )
        {
           switch (viewParams)
            {
                case "day":
                    {
                        ViewBag.Records = unitOfWork.Records.Get(record => record.DateTimeStart == DateTime.Today, ord => ord.OrderBy((x => x.DateTimeStart)));
                        return PartialView("_Records");
                    }
                 case "week":
                    {
                        var date = DateTime.Today.AddDays(7);
                        ViewBag.Records = unitOfWork.Records.Get(record => record.DateTimeStart >= DateTime.Today && record.DateTimeStart <= date, ord => ord.OrderBy((x => x.DateTimeStart)));
                        return PartialView("_Records");
                    }
                
                case "month":
                    {
                        var date = DateTime.Today.AddMonths(1);
                        ViewBag.Records = unitOfWork.Records.Get(record => record.DateTimeStart >= DateTime.Today && record.DateTimeStart <= date, ord => ord.OrderBy((x => x.DateTimeStart)));
                        return PartialView("_Records");
                    }
                   
                case "list":
                    {
                        ViewBag.Records = unitOfWork.Records.Get(null, ord => ord.OrderBy((x => x.DateTimeStart)));
                        return PartialView("_Records");
                    }
                   
            }
         
            return PartialView("_Records");
        }

        public ActionResult SearchRecords(string search, string date)
        {
            DateTime datetime;
            if(search=="") search = null;
           
            if (!DateTime.TryParse(date, out datetime))
            {
                ViewBag.Records =
                    unitOfWork.Records.Get(record => record.Theme.IndexOf(search) != -1 || record.Place.IndexOf(search) != -1,
                        ord => ord.OrderBy((x => x.DateTimeStart)));
            }
            else
            {
                ViewBag.Records = unitOfWork.Records.Get(record => (record.Theme.IndexOf(search) != -1 || record.Place.IndexOf(search) != -1) &&(record.DateTimeStart==datetime), ord => ord.OrderBy((x => x.DateTimeStart)));
            }
        
          
            return PartialView("_Records");
        }

        [HttpPost]
        public bool Done(int recordId)
        {
            var rec = unitOfWork.Records.Get(x => x.Id == recordId).FirstOrDefault();
            rec.Done = true;
            unitOfWork.Records.Update(rec);
            unitOfWork.Save();
            return true;

        }

        [HttpPost]
        public bool Delete(int recordId)
        {
            unitOfWork.Records.Delete(recordId);
            unitOfWork.Save();
            return true;

        }

     
        public ActionResult AddRecord()
        {
            ViewBag.Record = null;
            return PartialView("_Record");

        }

        [HttpPost]
        public string AddRecord(Records record)
        {
            try
            {
                unitOfWork.Records.Add(record);
                unitOfWork.Save();
                return "1";
            }
            catch (Exception)
            {

                return "-1";
            }
           
        }


        public ActionResult UpdateRecord(int recordId)
        {
            ViewBag.Record = unitOfWork.Records.Get(x => x.Id == recordId).FirstOrDefault();
            return PartialView("_Record");
        }

        [HttpPost]
        public string UpdateRecord(Records record)
        {
            try
            {
                var rec = unitOfWork.Records.Get(x => x.Id == record.Id).FirstOrDefault();
                rec.DateTimeStart = record.DateTimeStart;
                rec.DateTimeEnd = record.DateTimeEnd;
                rec.Place = record.Place;
                rec.Theme = record.Theme;
                unitOfWork.Records.Update(rec);
                unitOfWork.Save();
                return "1";
            }
            catch (Exception)
            {

                return "-1";
            }

        }

 
    }
}