﻿$(document).ready(function() {
    $('#viewParams').change(function () { //выбор записей на денб неделю и тд
        var txt = $(this).find('option:selected').text();
        $.ajax({
            type: 'GET',
            data: { viewParams: $(this).val() },
            url: '/Diary/GetRecords',
            success: function (data) {
                $("#records").empty().append(data);
                $("#diaryHead").html(txt);

            }
        });
    });

    $("body").on('click', '.TypesRecord span', function () { //фильтрация оп типу записей
        var id = $(this).data("id");
        if (id === "all") {
            $(".tr").show();
            return;
        }
        $(".tr[id!="+id+"]").hide();
        $("." + id).show();
    });

    $("body").on('input', '#searchRecord', function () { //поиск записей
        var txt = $(this).val();
        var date = $('#datepicker').val();
        if (txt.length < 2) return;
        $.ajax({
            type: 'GET',
            data: {
                search: txt,
                date: date
    },
            url: '/Diary/SearchRecords',
            success: function (data) {
                $("#records").empty().append(data);
                $("#diaryHead").html("Найденные записи");

            }
        });
    });

    $("body").on('change', '#datepicker', function () { //поиск записей
        var date = $(this).val();
        var txt = $('#searchRecord').val();
        $.ajax({
            type: 'GET',
            data: {
                search: txt,
                date: date
            },
            url: '/Diary/SearchRecords',
            success: function (data) {
                $("#records").empty().append(data);
                $("#diaryHead").html("Найденные записи");

            }
        });
    });

    $("body").on('click', '.Done', function (event) { //задача выполнена
        var elem = $(this);
        var id = elem.data("id");
        $.ajax({
            type: 'POST',
            data: {
                recordId: id
               
            },
            url: '/Diary/Done',
            success: function (data) {
                $("#" + id).css("background", "#baf2b4");
                elem.remove();

            }
        });
        event.stopImmediatePropagation();
    });

    $("body").on('click', '.Delete', function (event) { //задача удалить
        var elem = $(this);
        var id = elem.data("id");
        $.ajax({
            type: 'POST',
            data: {
                recordId: id

            },
            url: '/Diary/Delete',
            success: function (data) {
                $("#" + id).remove();
               
            }
        });
        event.stopImmediatePropagation();

    });

    $("body").on('click', '#addRecord', function () { // открыть окно для создания заметки
        $.ajax({
            type: 'GET',
            url: '/Diary/AddRecord',
            success: function (data) {
                $('.Popup').empty().append(data).show();
                $('.Overlay').show();

            }
        });
    });

    //скрыть попуп по клику на оверлей
    $('body').on('click', '.Overlay', function () {
        $(this).hide();
        $('.Popup').empty().hide();;

    });


    $('.PopupEditRecord').on('click', '#AddNewRecord', function () { //сохранить новую заметку
            $.ajax({
                type: 'POST',
                url: '/Diary/AddRecord',
                data: $('#addRecordForm').serialize(),
                success: function (data) {
                    if (data !== "-1") {
                        $('.Popup').empty().hide();
                        $('.Overlay').hide();
                       } else {
                        alert("Ошибка при сохранении");
                    }

                }

            });
    });


    $("body").on('click', '.tr', function () { // открыть окно для редактирования заметки
        var id = this.id;
        $.ajax({
            type: 'GET',
            url: '/Diary/UpdateRecord',
            data: {recordId: id},
            success: function (data) {
                $('.Popup').empty().append(data).show();
                $('.Overlay').show();

            }
        });
    });
    $('.PopupEditRecord').on('click', '#UpdateRecord', function () { //сохранить изменения заметки
        $.ajax({
            type: 'POST',
            url: '/Diary/UpdateRecord',
            data: $('#addRecordForm').serialize(),
            success: function (data) {
                if (data !== "-1") {
                    $('.Popup').empty().hide();
                    $('.Overlay').hide();
             
                } else {
                    alert("Ошибка при сохранении");
                }

            }

        });
    });


    $("body").on('input', '#SearchContact', function () { //поиск контактов
        var txt = $(this).val();
        $.ajax({
            type: 'GET',
            data: {
                search: txt
            },
            url: '/Contact/SearchContact',
            success: function (data) {
                $("#contact").empty().append(data);
                
            }
        });
    });


    $("body").on('click', '#AddContact', function () { // открыть окно для создания контакта
        $.ajax({
            type: 'GET',
            url: '/Contact/AddContact',
            success: function (data) {
                $('.Popup').empty().append(data).show();
                $('.Overlay').show();

            }
        });
    });


    $('.Popup').on('click', '#AddNewContact', function () { //сохранить  
        $.ajax({
            type: 'POST',
            url: '/Contact/AddContact',
            data: $('#addForm').serialize(),
            success: function (data) {
                    $('.Popup').empty().hide();
                    $('.Overlay').hide();
                    $("#contact").empty().append(data);

            }

        });
    });

    $("body").on('click', '.updateImg', function (event) { // открыть окно для изменения контакта
        event.stopImmediatePropagation();
        var id = this.id;
        $.ajax({
            type: 'GET',
            url: '/Contact/UpdateContact',
            data: {
                contactId: id
            },
            success: function (data) {
                $('.Popup').empty().append(data).show();
                $('.Overlay').show();

            }
        });
      
    });

    $('.Popup').on('click', '#SaveUpdateContact', function () { //сохранить изменения контакта
        $.ajax({
            type: 'POST',
            url: '/Contact/UpdateContact',
            data: $('#addForm').serialize(),
            success: function (data) {
                $('.Popup').empty().hide();
                $('.Overlay').hide();
                $("#contact").empty().append(data);

            }

        });
    });

    $("body").on('click', '.RedButton', function (event) { // удаление контакта
       var id = this.id;
        $.ajax({
            type: 'POST',
            url: '/Contact/DeleteContact',
            data: {
                contactId: id
            },
            success: function () {
                $('.Popup').empty().hide();
                $('.Overlay').hide();
                $("#contact").find('.'+id+'-del').remove();

            }
        });

    });

    $('#datepicker').datepicker($.extend({
        inline: true,
        changeYear: true,
        changeMonth: true,
    },
         $.datepicker.regional['ru']
       ));

});

jQuery(function ($) {
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
        'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
        'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekHeader: 'Нед',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
});